// [Section] If-else statements

let numA = -1;

/*
	Syntax:
		if(condition){
			statement/s;
		}
*/

if (numA < 0){
	console.log("Hello");
}

console.log(numA < 0);

let city = "Mumbai";

if (city === "mumbai"){
	console.log("Welcome to Mumbai");
}
else{
	console.log("Not welcome!")
}

/*
	else if statement
	Syntax:
		if(condition){
			statement/s;
		}
		else if(condition){
			statement/s;
		}
*/

let numH = 1;

if (numA > 0){
	console.log("Hello");
}
else if(numH > 0){
	console.log("Onkar");
}

city = "Mumbai";

if(city === "Chennai"){
	console.log("Welcome to Chennai");
}
else if(city === "Mumbai"){
	console.log("Welcome to Mumbai");
}
else{
	console.log("City not included");
}

// Function containing if else statements

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return 'Not a typhoon yet.';
	}
	else if(windspeed <= 61){
		return 'Tropical depression detected.';
	}
	else if(windspeed >= 62 && windspeed <= 88){
		return 'Tropical storm detected.';
	}
	else if(windspeed >= 89 && windspeed <= 117){
		return 'Severe Tropical storm detected.';
	}
	else{
		return 'Typhoon detected.';
	}
}

let message = determineTyphoonIntensity(70);
console.log(message);

// Condition (Ternary) Operator

/*
	Syntax:
		(condition) ? true : false;
*/

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: "+ternaryResult);

let ternaryResult1 = (1 < 18) ? "Value is less than 18" : "Value is greater than 18";
console.log("Result of Ternary Operator: "+ternaryResult1);

// Multiple statement execution

let name;

function isOfLegalAge(){
	name = "Onkar";
	return 'You are of legal age limit';
}

function isUnderAge(){
	name = "Aarush";
	return 'You are under legal age limit';
}

let age = parseInt(prompt("What is your age?")); 
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

// [Section] Switch statement - Evaluates an expression and matches the expression's value to a case clause.
/*
	Syntax:
		switch (expression){
			case value1:
				statement/s;
				break;
			case value2:
				statement/s;
				break;
			default:
				statement/s;
		}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red");
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thrusday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");	
}


// [Section] Try-Catch_Finally Statement

function showIntensityAlert(windspeed){
	try {
		alert(determineTyphoonIntensity(windspeed))
	}
	catch (error){
		console.log(typeof error);

		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(120);